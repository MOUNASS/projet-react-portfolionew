const Portfolio_data = [
  {
    id: 1,
    category: "DEVELOPMENT",
    totalLike: "600",
    title: "The services provide for design ",
    image: "https://cdn.pixabay.com/photo/2015/12/04/14/05/code-1076536__340.jpg"
  },
  {
    id: 2,
    category: "APPLICATION",
    totalLike: "750",
    title: "Mobile app landing design & maintain",
    image: "https://cdn.pixabay.com/photo/2015/02/11/13/01/ipad-632394__340.jpg",
  },
  {
    id: 3,
    category: "PHOTOSHOP",
    totalLike: "630",
    title: "Logo design creativity & Application ",
    image: "https://cdn.pixabay.com/photo/2017/12/29/22/10/background-3048816__340.jpg",
  },
  {
    id: 4,
    category: "FIGMA",
    totalLike: "360",
    title: "Mobile app landing design & Services",
    image: "https://media.istockphoto.com/photos/developing-programmer-development-website-design-and-coding-working-picture-id1152943618?k=20&m=1152943618&s=612x612&w=0&h=CO0bNf9bjbvzTllpPnzSZYFGt1U-IGrypB0rxA2kJqE=",
  },
  {
    id: 5,
    category: "WEB DESIGN",
    totalLike: "280",
    title: "Design for tecnology & services",
    image: "https://media.istockphoto.com/vectors/software-development-web-coding-on-laptop-synchronization-work-data-vector-id1252856954?k=20&m=1252856954&s=612x612&w=0&h=dIq6JmZ0waO14u2vCkNs33czci_8-Y9tov68E9oSZkQ=",
  },
  {
    id: 6,
    category: "WEB DESIGN",
    totalLike: "690",
    title: "App for tecnology & services",
    image: "https://media.istockphoto.com/photos/home-office-set-up-with-responsive-snow-mountain-website-picture-id1224339510?k=20&m=1224339510&s=612x612&w=0&h=8tWdmWYurMHZ3Sx6AS8eo-WzXEm0K4HrqCcdDf2Ermk=",
  },
]
export default Portfolio_data
